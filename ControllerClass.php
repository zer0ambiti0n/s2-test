<?php
class ControllerClass {
    //TODO: вынести маппинг в отдельный класс
    const CUSTOM_FIELD_NAME_ID_MAP = [
        'acceptReceivedOptions' => '98127',
        'discounts' => '98269',
        'checkStatusOptions' => '85871',
        'citizenshipOptions' => '78871',
        'documentTypeOptions' => '82893',
        'genderOptions' => '77777',
        'addressMatchLivingPlaceOptions' => '79078',
        'secondNameChangedOptions' => '83804',
        'certificateOptions' => '94580',
        'countryOptions' => '94698',
        'cityOptions' => '94699',
        'directionOptions' => '94705',
        'specializationOptions' => '94706',
        'educationLevelOptions' => '94600',
        'educationFormOptions' => '94607',
        'educationStatusOptions' => '94608',
        'educationTypeOptions' => '94618',
    ];

    //Выбираем из JSON ответа поля сделки
    //TODO: DealsWidgetRequest
    public function dealsWidget(Request $request): view
    {
        $dealId = $request->input('ids')[0] ?? null;
        $deal = $this->repository->getDeal($dealId);

        $contactIds = $this->getIdListFromDataArray($deal['relationships']['contacts']['data']);
        if ($contactIds)
            $contacts = $this->repository->getContactsByIdList($contactIds);

        $productIds = $this->getIdListFromDataArray($deal['relationships']['products']['data']);
        if ($productIds)
            $products = $this->repository->getProductsByIdList($productIds);

        return view('CPKPP.widget', [
            'deal' => $deal,
            'contacts' => $contacts ?? [],
            'products' => $products ?? [],
            +
           $this->mapCustomFieldsDetails([
               'acceptReceivedOptions',
               'discounts',
               'checkStatusOptions',
               'citizenshipOptions',
               'documentTypeOptions',
               'genderOptions',
               'addressMatchLivingPlaceOptions',
               'secondNameChangedOptions',
               'certificateOptions',
               'countryOptions',
               'cityOptions',
               'directionOptions',
               'specializationOptions',
               'educationLevelOptions',
               'educationFormOptions',
               'educationStatusOptions',
               'educationTypeOptions',
           ])
        ]);
    }

    /**
     * @param array<string> $fieldNames
     *
     * @return array
     */
    private function mapCustomFieldsDetails(array $fieldNames): array
    {
        $result = [];

        foreach ($fieldNames as $fieldName) {
            $result[$fieldName] = isset(self::CUSTOM_FIELD_NAME_ID_MAP[$fieldName]) ?
                $this->getCustomFieldDetail(self::CUSTOM_FIELD_NAME_ID_MAP[$fieldName]) : null;
        }

        return $result;
    }

    private function getCustomFieldDetail(string $fieldId): mixed
    {
        return $this->repository->getCustomFieldDetail($fieldId)['data']['attributes']['select-options'] ?? null;
    }
}